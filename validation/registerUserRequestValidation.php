<?php

function registerUserRequestValidation(User $user)
{
    $params['username'] = filter_var($_POST['username'], FILTER_SANITIZE_STRING);
    if (empty($params['username'])) {
        throw new Exception("Invalid username, username cannot be empty.");
    }
    try {
        if (!$user->checkUsernameAvailability($params['username']))
            throw new Exception("Username already exists.", 400);
    } catch (Exception $e) {
        throw $e;
    }

    $params['email'] = strtolower(filter_var($_POST['email'], FILTER_SANITIZE_EMAIL));
    if (!filter_var($params['email'], FILTER_VALIDATE_EMAIL)) {
        throw new Exception("Invalid email.");
    }
    try {
        if (!$user->checkEmailUniqueness($params['email']))
            throw new Exception("Email already exists.", 400);
    } catch (Exception $e) {
        throw $e;
    }

    $params['password'] = $_POST['password'];
    if ((strlen($params['password']) < 8) || strlen($params['password']) > 32) {
        throw new Exception("Invalid password length.");
    }

    return $params;
}
