<?php

require_once '../models/User.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    try {
        $user = new User($config);
        if ($user->checkUsernameAvailability($_POST['username']))
            echo json_encode(true);
        else echo json_encode(false);
    } catch (Exception $e) {
        echo json_encode(['error' => 'An error occurred while checking username availability.']);
        exit();
    }
}