/*function launch_toast(key, message) {
    const toast = document.getElementById("toast");
    const title = document.getElementById("title");
    const description = document.getElementById("desc");
    toast.className = "show";
    title.innerText = key;
    if (key === 'success') {
        title.style.backgroundColor = 'darkgreen';
        toast.style.backgroundColor = 'green';
    } else if (key === 'error') {
        title.style.backgroundColor = 'darkred';
        toast.style.backgroundColor = 'red';
    }
    description.innerText = message;
    setTimeout(function () {
        toast.className = toast.className.replace("show", "");
    }, 5000);
}*/

$(document).ready(function () {
    $('.register-form').validate({
        rules: {
            username: {
                required: true,
                remote: {
                    url: 'validation/clientSideUsernameValidation.php', // URL to check if username is unique
                    type: 'post',
                    datatype: 'json',
                    data: {
                        username: function () {
                            return $('#username').val();
                        }
                    }
                }
            },
            email: {
                required: true,
                email: true,
                remote: {
                    url: 'validation/clientSideEmailValidation.php', // URL to check if email is unique
                    type: 'post',
                    datatype: 'json',
                    data: {
                        email: function () {
                            return $('#email').val().toLowerCase();
                        }
                    }
                }
            },
            password: {
                required: true,
                minlength: 8,
                maxlength: 32
            }
        },
        messages: {
            username: {
                remote: 'Username is already taken.'
            },
            email: {
                remote: 'Email is already taken.'
            }
        },
        submitHandler: function (form) {
            const formData = $(form).serialize();
            $.ajax({
                url: 'controllers/registerController.php',
                type: 'POST',
                dataType: 'json',
                data: formData,
                success: function (response) {
                    if (response.error) {
                        alert(response.error);
                        // launch_toast('error', response.error);
                    } else {
                        alert(response.message + "\n" + "UserID: " + response.payload.id + "\n" + "Username: " + response.payload.username  + "\n" + "Email: " + response.payload.email  + "\n" + "Registration Data: " + response.payload.registration_date);
                        window.location.href = 'views/html/thankyou.html';
                        /*launch_toast('success', response.message);
                        setTimeout(function () {
                            window.location.href = 'views/html/thankyou.html';
                        }, 5000);*/
                    }
                },
                error: function (xhr, status, error) {
                    if (xhr.responseText)
                        alert(JSON.parse(xhr.responseText).error);
                    else alert('An error occurred while processing the request, please try again later.');
                    // launch_toast('error', JSON.parse(xhr.responseText).error);
                    // else launch_toast('error', 'An error occurred while processing the request, please try again later.')
                }
            });
        }
    });
});

$(document).ready(function () {
    $(".search-form").submit(function (event) {
        event.preventDefault();
        const username = $("#search-text").val();
        $.get("controllers/userSearchController.php", {username: username}, function (response) {
            if (response === undefined) {
                const registerPage = document.getElementById('register-page');
                registerPage.style.display = 'none';
                const main = document.getElementById('main');
                main.innerText = 'Sorry, it seams there no users with that name...';
            }
            else if(response.error){
                alert(response.error);
            }
            else{
                const registerPage = document.getElementById('register-page');
                registerPage.style.display = 'none';
                const tbody = document.querySelector("tbody");
                let tableContent = '';
                response.payload.forEach(function (userObj) {
                    let tr = "<tr>";
                    for (let key in userObj) {
                        let td = "<td>" + userObj[key] + "</td>";
                        tr += td;
                    }
                    tr += "</tr>"
                    tableContent += tr;
                });
                tbody.innerHTML = tableContent;
                const searchResults = document.getElementById('search-results');
                searchResults.style.display = 'block';
            }
        }, "json")
            .fail(function (xhr, status, error) {
                alert("AJAX request failed: " + error);
            });
    });
});

let password = document.getElementById("password");
let power = document.getElementById("power-point");
password.oninput = function () {
    let point = 0;
    let value = password.value;
    let widthPower =
        ["1%", "25%", "50%", "75%", "100%"];
    let colorPower =
        ["#D73F40", "#DC6551", "#F2B84F", "#BDE952", "#3ba62f"];

    if (value.length >= 6) {
        let arrayTest =
            [/[0-9]/, /[a-z]/, /[A-Z]/, /[^0-9a-zA-Z]/];
        arrayTest.forEach((item) => {
            if (item.test(value)) {
                point += 1;
            }
        });
    }
    power.style.width = widthPower[point];
    power.style.backgroundColor = colorPower[point];
};

