<?php

require_once 'Connection.php';
$config = require_once '../config/database.php';
require_once '../helper.php';
require_once '../header.php';

class UsersTable
{
    private $connection;

    public function __construct($config)
    {
        $this->connection = (new Connection($config))->connect();
    }

    public function build()
    {
        try {
            if ($this->connection->exec(
                "CREATE TABLE IF NOT EXISTS `users` (
                            `id` INT AUTO_INCREMENT NOT NULL,
                            `username` varchar(255) NOT NULL,
                            `email` varchar(255) NOT NULL,
                            `password` varchar(255) NOT NULL,
                            `registration_time` DATETIME NOT NULL,
                            PRIMARY KEY (`id`),
                            UNIQUE (`email`),
                            UNIQUE (`username`)
            );") === false)
                echo failureResponse("An error occurred while creating the table, please try again later.", 500);
            else echo successResponse("Users table has been created successfully.", 201);
        } catch (PDOException $e) {
            echo failureResponse($e->getMessage(), $e->getCode());
            exit();
        }
    }
}

(new UsersTable($config))->build();




