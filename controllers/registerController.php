<?php

require_once '../validation/registerUserRequestValidation.php';
require_once '../models/User.php';
require_once '../helper.php';
require_once '../header.php';
$redisConfig = require_once '../config/redis.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    try {
        $user = new User($config);
        $params = registerUserRequestValidation($user);
        if ($user = $user->createUser($params)) {
            $redis = new Redis();
            $redis->connect($redisConfig['host'], $redisConfig['port']);
            $userClone['id'] = $user->id;
            $userClone['username'] = $user->username;
            $userClone['email'] = $user->email;
            unset($userClone->registration_date);
            $redis->set(strtolower($user->username), serialize($userClone));
            echo successResponse("User has been created successfully", 201, $user);
        } else throw new Exception("An error occurred while processing the request, please try gain later", 500);
    } catch (Exception $e) {
        echo failureResponse($e->getMessage(), $e->getCode());
        exit();
    }
}