<?php

function successResponse($message, $responseCode, $payload = null){
    http_response_code($responseCode);
    return json_encode([
        'message' => $message,
        'payload' => $payload
    ]);
}

function failureResponse($error, $responseCode){
    http_response_code($responseCode);
    return json_encode([
        'error' => $error
    ]);
}