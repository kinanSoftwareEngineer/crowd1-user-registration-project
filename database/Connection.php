<?php

require_once '../helper.php';
require_once '../header.php';

class Connection
{

    private $host;
    private $port;
    private $databaseName;
    private $user;
    private $password;
    private $charset;

    public function __construct($config)
    {
        $this->host = $config['host'];
        $this->databaseName = $config['databaseName'];
        $this->user = $config['user'];
        $this->password = $config['password'];
        $this->charset = $config['charset'];
        $this->port = $config['port'];
    }

    public function connect()
    {
        $dsn = "mysql:host=$this->host;port=$this->port;dbname=$this->databaseName;charset=$this->charset";
        try {
            return new PDO($dsn, $this->user, $this->password, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
        } catch (PDOException $e) {
            echo failureResponse($e->getMessage(), $e->getCode());
            exit();
        }
    }
}