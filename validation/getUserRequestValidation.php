<?php

function getUserRequestValidation() {
    $username = filter_var($_GET['username'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
    if (empty($username)) {
        throw new Exception("Username cannot be empty.");
    }else return $username;
}
