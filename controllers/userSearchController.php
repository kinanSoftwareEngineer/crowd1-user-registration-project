<?php

require_once '../validation/getUserRequestValidation.php';
require_once '../models/User.php';
require_once '../helper.php';
require_once '../header.php';
$redisConfig = require_once '../config/redis.php';

if ($_SERVER["REQUEST_METHOD"] == "GET") {
    try {
        $username = getUserRequestValidation();
//        $pageNumber = getUserRequestValidation()['pageNumber'];
        $redis = new Redis();
        $redis->connect($redisConfig['host'], $redisConfig['port']);
        $keys = $redis->keys(strtolower($username) . '*');
        /*$totalResults = 0;
        $payload = [];
        $result = [];*/
        if ($keys) {
            $result = [];
            foreach ($keys as $key) {
                $result[] = unserialize($redis->get($key));
//                $totalResults++;
            }
        }
        /*$payload['data'] = $result;
        $payload['current_page'] = $pageNumber;
        $payload['total_results'] = $totalResults;
        $payload['total_pages'] = ceil($totalResults / 10);
        $payload['results_per_page'] = 10;*/
        if (!empty($result)) {
            echo successResponse("Users have been retrieved successfully.", 200, $result);
        } else throw new Exception("Username not found.", 204);
    } catch (Exception $e) {
        echo failureResponse($e->getMessage(), $e->getCode());
        exit();
    }
}