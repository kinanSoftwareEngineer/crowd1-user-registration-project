<?php

return [
    'host' => 'localhost',
    'port' => 3306,
    'databaseName' => 'crowd1_assignment',
    'user' => 'root',
    'password' => '',
    'charset' => 'UTF8'
];