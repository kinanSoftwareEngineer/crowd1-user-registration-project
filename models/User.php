<?php

require_once '../database/Connection.php';
$config = require_once '../config/database.php';

class User
{

    private $connection;
    private $password;
    public $id;
    public $email;
    public $username;
    public $registration_date;

    public function __construct($config)
    {
        $this->connection = (new Connection($config))->connect();
        $this->password = null;
        $this->id = null;
        $this->email = null;
        $this->username = null;
        $this->registration_date = null;
    }

    public function search($username)
    {
        try {
            $statement = $this->connection->prepare("SELECT `id`, `username`, `email` FROM `users` WHERE `username` LIKE :username");
            $statement->bindValue(':username', $username . '%', PDO::PARAM_STR);
            $statement->execute();
            return $statement;
        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function checkUsernameAvailability($username)
    {
        try {
            $statement = $this->connection->prepare("SELECT `username` FROM `users` WHERE `username` LIKE :username");
            $statement->bindValue(':username', $username, PDO::PARAM_STR);
            $statement->execute();
            return $statement->rowCount() == 0;
        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function checkEmailUniqueness($email)
    {
        try {
            $statement = $this->connection->prepare("SELECT `email` FROM `users` WHERE `email` LIKE :email");
            $statement->bindValue(':email', $email, PDO::PARAM_STR);
            $statement->execute();
            return $statement->rowCount() == 0;
        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function createUser($params)
    {
        $currentDate = date("Y-m-d h:i:sa");
        $password = password_hash($params['password'], PASSWORD_BCRYPT);
        try {
            $statement = $this->connection->prepare("INSERT INTO users (username, email, password, registration_time) VALUES (?, ?, ?, ?)");
            if($statement->execute([$params['username'], $params['email'], $password, $currentDate])){
                $this->username = $params['username'];
                $this->email = $params['email'];
                $this->registration_date = $currentDate;
                $statement = $this->connection->prepare("SELECT `id` FROM `users` WHERE `email` like :email");
                $statement->bindValue(':email', $this->email);
                $statement->execute();
                $this->id = intval($statement->fetch(PDO::FETCH_ASSOC)['id']);
                $this->connection = null;
                return $this;
            } else return null;
        } catch (PDOException $e) {
            throw $e;
        }
    }
}



